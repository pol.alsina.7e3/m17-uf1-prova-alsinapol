using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShipBound : MonoBehaviour
{
    public float minXBound;
    public float maxXBound;
    public float minYBound;
    public float maxYBound;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        PlayerBoundaries();
    }
    private void PlayerBoundaries()
    {
        if (transform.position.x >= minXBound)
        {
            transform.position = new Vector3(minXBound, transform.position.y, 0);
        }
        else if (transform.position.x <= maxXBound)
        {
            transform.position = new Vector3(maxXBound, transform.position.y, 0);
        }
        else if (transform.position.x <= minYBound)
        {
            transform.position = new Vector3(transform.position.x, minYBound, 0);
        }
        else if (transform.position.x <= maxYBound)
        {
            transform.position = new Vector3(transform.position.x, maxYBound, 0);
        }
    }
}
