using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    private float _time;
    public float _interval;
    public GameObject objectToCreate;
    public float[] limitsX;
    // Start is called before the first frame update
    void Start()
    {
        //Instantiate(this.gameObject, new Vector3(GenerateRandom(), limitsY, 0), Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        _time += Time.deltaTime;
        if (_time > _interval)
        {

            Instantiate(objectToCreate, new Vector3(GenerateRandom(), transform.position.y, 0), Quaternion.identity);
            objectToCreate.SetActive(true);
            _time = 0;
        }
    }
    private float GenerateRandom()
    {
        return Random.Range(limitsX[0], limitsX[1]);
    }

}
