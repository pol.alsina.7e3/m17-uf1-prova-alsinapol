using UnityEngine;

public class PlayerShipMove : MonoBehaviour
{
    private float _horizontal;
    private float _vertical;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _horizontal = Input.GetAxisRaw("Horizontal");
        _vertical = Input.GetAxis("Vertical");
        Move();
    }
    private void Move()
    {
        transform.position = new Vector3(speed * _horizontal + transform.position.x, speed * _vertical + transform.position.y, transform.position.z);
        //transform.position = new Vector3(speed * _vertical + transform.position.x, transform.position.y, transform.position.z);
    }
}
