using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionLimit : MonoBehaviour
{
    private void Update()
    {
        if (transform.position.y < -5)
        {
            Destroy(gameObject);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("limit"))
        {
            Destroy(gameObject);
        }
    }
}
